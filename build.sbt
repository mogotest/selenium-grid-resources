name := "Mogotest Selenium Grid Resources"

version := "1.6.0"

scalaVersion := "2.10.2"

libraryDependencies ++= Seq(
  "org.seleniumhq.selenium" % "selenium-server" % "2.43.0",
  "org.scalaj" %% "scalaj-http" % "0.3.10"
)
