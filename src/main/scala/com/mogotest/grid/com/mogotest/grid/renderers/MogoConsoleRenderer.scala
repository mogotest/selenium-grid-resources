package com.mogotest.grid.com.mogotest.grid.renderers

import com.mogotest.grid.proxies.MogoRemoteProxy
import org.openqa.grid.internal.utils.HtmlRenderer
import scala.collection.JavaConversions._
import org.openqa.grid.web.utils.BrowserNameUtils
import org.openqa.selenium.remote.DesiredCapabilities
import org.openqa.grid.selenium.proxy.DefaultRemoteProxy

class MogoConsoleRenderer(proxy: MogoRemoteProxy) extends HtmlRenderer
{
  def renderSummary() =
  {
    val builder = new StringBuilder

    builder.append("<fieldset>")
    builder.append("<legend>").append(proxy.getClass.getSimpleName).append("</legend>")
    builder.append("listening on ").append(proxy.canonicalRemoteURL)

    if ((proxy.asInstanceOf[DefaultRemoteProxy]).isDown)
      builder.append("(cannot be reached at the moment)")

    builder.append("<br />")

    if (proxy.getTimeOut > 0)
    {
      val inSec = proxy.getTimeOut / 1000
      builder.append("test session time out after ").append(inSec).append(" sec.<br />")
    }

    builder.append("Supports up to <b>").append(proxy.getMaxNumberOfConcurrentTestSessions).append("</b> concurrent tests from: <br />")

    for (slot <- proxy.getTestSlots) {
      val session = slot.getSession
      val icon = getIcon(slot.getCapabilities)

      if (icon != null) {
        builder.append("<img ")
        builder.append("src='").append(icon).append("' ")
      }
      else {
        builder.append("<a href='#' ")
      }

      if (session != null) {
        builder.append(" class='busy' ")
        builder.append(" title='").append(session.get("lastCommand")).append("' ")
      }
      else {
        builder.append(" title='").append(slot.getCapabilities).append("' ")
      }

      if (icon != null) {
        builder.append("/>")
      }
      else {
        builder.append(">")
        builder.append(slot.getCapabilities.get("browserName"))
        builder.append("</a>")
      }
    }

    builder.append("</fieldset>")

    builder.toString
  }

  def getIcon(capabilities: java.util.Map[String, AnyRef]) =
    BrowserNameUtils.getConsoleIconPath(new DesiredCapabilities(capabilities), proxy.getRegistry)
}