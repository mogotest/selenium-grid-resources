package com.mogotest.grid.servlets

import com.google.common.io.ByteStreams
import java.io.ByteArrayInputStream
import javax.servlet.http.{HttpServletResponse, HttpServletRequest}
import org.openqa.grid.internal.{RemoteProxy, Registry}
import org.openqa.grid.web.servlet.RegistryBasedServlet
import scala.collection.mutable.Map
import scala.collection.JavaConversions._
import scala.xml._
import com.mogotest.grid.proxies.MogoRemoteProxy
import org.openqa.selenium.remote.DesiredCapabilities

class Console(registry: Registry) extends RegistryBasedServlet(registry)
{
  def this() = this(null)

  override def doGet(request: HttpServletRequest, response: HttpServletResponse) =
  {
    response.setContentType("text/html")
    response.setCharacterEncoding("UTF-8")
    response.setStatus(200)

    val proxyGroups = Map[String, List[RemoteProxy]]()

    // Look through each proxy and group by browser name.
    for (proxy <- getRegistry.getAllProxies)
      for (slot <- proxy.getTestSlots)
      {
        val browserName = slot.getCapabilities()("browserName").asInstanceOf[String]

        proxyGroups(browserName) = proxy :: proxyGroups.getOrElse(browserName, Nil)
      }

    // Now look through each proxy group and sort by connection URLs.
    for ((browserName, proxyList) <- proxyGroups)
      proxyGroups(browserName) = proxyList.sortWith(_.asInstanceOf[MogoRemoteProxy].canonicalRemoteURL.toString < _.asInstanceOf[MogoRemoteProxy].canonicalRemoteURL.toString)

    // Group each pending request by browser name so we can display them in the UI relative to the test slots for that group.
    val requestGroups = Map[String, List[DesiredCapabilities]]()
    for (caps <- getRegistry.getDesiredCapabilities)
      requestGroups(caps.getBrowserName) = caps :: requestGroups.getOrElse(caps.getBrowserName, Nil)

    val output =
      <html>
        <head>
          <meta http-equiv='refresh' content='3' />
          <title>Grid Overview</title>

          <style>
            .busy {{
              opacity: 0.4;
              Filter: AlphaComposite(opacity=40);
            }}
          </style>
        </head>

        <body>
          <h1>Grid Hub</h1>

          { proxyGroups.keys.toList.sorted.map(browserName => renderProxyGroup(browserName, proxyGroups, requestGroups)) }
        </body>
      </html>

    val in = new ByteArrayInputStream(Xhtml.toXhtml(output).toString.getBytes("UTF-8"))
    try
    {
      ByteStreams.copy(in, response.getOutputStream)
    }
    finally
    {
      in.close
      response.getOutputStream.close
    }
  }

  def renderProxyGroup(browserName: String, proxyGroups: Map[String, List[RemoteProxy]], requestGroups: Map[String, List[DesiredCapabilities]]) =
    <fieldset style='width: 45%; border: 2px solid black; float: left; margin-bottom: 40px; margin-right: 20px;'>
      <legend>{ { "%s: %d total".format(browserName, proxyGroups(browserName).size) } }</legend>
      { proxyGroups(browserName).map(proxy => XML.loadString(proxy.getHtmlRender.renderSummary)) }

      { renderRequests(requestGroups.getOrElse(browserName, Nil)) }
    </fieldset>

  def renderRequests(requests: List[DesiredCapabilities]) =
    if (requests == Nil)
      <div style='margin-top: 20px;'>
        <em>No pending requests.</em>
      </div>
    else
      <div style='margin-top: 20px;'>
        { requests.size } requests waiting for a slot to be free.
        <ul>
          { requests.map(request => <li>{ request }</li>) }
        </ul>
      </div>
}