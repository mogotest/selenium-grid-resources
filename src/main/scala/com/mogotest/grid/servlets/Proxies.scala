package com.mogotest.grid.servlets

import org.openqa.grid.web.servlet.RegistryBasedServlet
import org.openqa.grid.internal.Registry
import javax.servlet.http.{HttpServletResponse, HttpServletRequest}
import org.json.{JSONArray, JSONObject}
import com.mogotest.grid.proxies.MogoRemoteProxy
import scala.collection.JavaConversions._

class Proxies(registry: Registry) extends RegistryBasedServlet(registry)
{
  def this() = this(null)

  override def doGet(request: HttpServletRequest, response: HttpServletResponse) =
  {
    response.setContentType("application/json")
    response.setCharacterEncoding("UTF-8")
    response.setStatus(200)

    val res = new JSONArray

    for (proxy <- getRegistry.getAllProxies)
    {
      val encodedProxy = new JSONObject
      encodedProxy.put(proxy.asInstanceOf[MogoRemoteProxy].canonicalRemoteURL.getHost, proxy.getId)
      res.put(encodedProxy)
    }

    response.getWriter.print(res)
    response.getWriter.close
  }

  override def doDelete(request: HttpServletRequest, response: HttpServletResponse) =
  {
    response.setContentType("application/json")
    response.setCharacterEncoding("UTF-8")

    val id = request.getParameter("id")

    val proxy = getRegistry.getAllProxies.getProxyById(id)

    if (proxy != null)
    {
      getRegistry.removeIfPresent(proxy)

      response.setStatus(204)
    }
    else
    {
      response.setStatus(404)
    }
  }
}
