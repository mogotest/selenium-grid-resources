package com.mogotest.grid.servlets

import org.openqa.grid.web.servlet.RegistryBasedServlet
import javax.servlet.http.{HttpServletResponse, HttpServletRequest}
import scala.collection.JavaConversions._
import org.json.{JSONObject, JSONArray}
import com.mogotest.grid.proxies.MogoRemoteProxy
import org.openqa.grid.internal.{ExternalSessionKey, RemoteProxy, Registry}

class Sessions(registry: Registry) extends RegistryBasedServlet(registry)
{
  def this() = this(null)

  override def doGet(request: HttpServletRequest, response: HttpServletResponse) =
  {
    response.setContentType("application/json")
    response.setCharacterEncoding("UTF-8")
    response.setStatus(200)

    val res = getResponse(request)
    response.getWriter.print(res)
    response.getWriter.close()
  }

  def getResponse(request: HttpServletRequest) =
  {
    val res = new JSONObject

    val sessionId = request.getParameter("sessionId")
    if (sessionId == null)
      res.put("nodes", encodeAllProxies)
    else
      res.put("node", encodeProxy(new ExternalSessionKey(sessionId), res))
  }

  def encodeAllProxies =
  {
    val encodedProxies = new JSONArray

    getRegistry.getAllProxies.foreach(proxy => encodedProxies.put(encodeProxy(proxy)))

    encodedProxies
  }

  def encodeProxy(sessionId: ExternalSessionKey, res: JSONObject) : JSONObject =
  {
    val session = getRegistry.getSession(sessionId)

    if (session != null)
      encodeProxy(session.getSlot.getProxy)
    else
      new JSONObject
  }

  def encodeProxy(proxy: RemoteProxy) : JSONObject =
  {
    val encodedProxy = new JSONObject
    val encodedTestSessions = new JSONArray

    for (slot <- proxy.getTestSlots)
    {
      val encodedTestSession = new JSONObject

      val session = slot.getSession
      if (session != null)
      {
        encodedTestSession.put("sessionId", session.getExternalKey)
        encodedTestSession.put("inactivityTimeInMilliseconds", session.getInactivityTime)
        encodedTestSession.put("browserName", session.getRequestedCapabilities()("browserName"))

        if (session.getRequestedCapabilities.containsKey("version"))
          encodedTestSession.put("version", session.getRequestedCapabilities()("version"))

        encodedTestSessions.put(encodedTestSession)
      }
    }

    encodedProxy.put("remoteUrl", proxy.getRemoteHost)
    encodedProxy.put("canonicalRemoteUrl", proxy.asInstanceOf[MogoRemoteProxy].canonicalRemoteURL)
    encodedProxy.put("host", proxy.asInstanceOf[MogoRemoteProxy].canonicalRemoteURL.getHost)
    encodedProxy.put("maxTimeOutInMilliseconds", proxy.getTimeOut)
    encodedProxy.put("activeSessions", encodedTestSessions)

    encodedProxy
  }
}