package com.mogotest.grid.servlets

import org.openqa.grid.web.servlet.RegistryBasedServlet
import javax.servlet.http.{HttpServletResponse, HttpServletRequest}
import scala.collection.JavaConversions._
import org.json.JSONObject
import scala.collection.mutable.Map
import org.openqa.grid.internal.Registry

class AvailableBrowsers(registry: Registry) extends RegistryBasedServlet(registry)
{
  def this() = this(null)

  override def doGet(request: HttpServletRequest, response: HttpServletResponse) =
  {
    response.setContentType("application/json")
    response.setCharacterEncoding("UTF-8")
    response.setStatus(200)

    val res = getResponse(request)
    response.getWriter.print(res)
    response.getWriter.close()
  }

  def getResponse(request: HttpServletRequest) =
  {
    val counts = Map[(String, String), Int]()

    // Check each slot in each proxy to see whether it has a session.  If not, then the slot is available for a
    // new session.
    for (proxy <- getRegistry.getAllProxies)
    {
      val proxyCapacity = proxy.getMaxNumberOfConcurrentTestSessions() - proxy.getTotalUsed()
      val countsPerProxy = Map[(String, String), Int]()

      for (slot <- proxy.getTestSlots)
        if (slot.getSession == null)
        {
          val browserName = slot.getCapabilities()("browserName").asInstanceOf[String]

          val version =
            if (slot.getCapabilities.containsKey("version"))
              slot.getCapabilities()("version").asInstanceOf[String]
            else
              ""

          val currentCount = countsPerProxy.getOrElse((browserName, version), 0)
          countsPerProxy((browserName, version)) = currentCount + 1
        }

      for (((browserName, version), count) <- countsPerProxy)
      {
        val currentCount = counts.getOrElse((browserName, version), 0)
        counts((browserName, version)) = currentCount + List(count, proxyCapacity).min
      }
    }

    val res = new JSONObject

    // Now that we have our map of counts, convert to JSON.
    for (((browserName, version), count) <- counts)
    {
      if (!res.has(browserName))
      {
        res.put(browserName, new JSONObject)
      }

      val versionMap = res.get(browserName).asInstanceOf[JSONObject]
      versionMap.put(version, count)
    }

    res
  }
}