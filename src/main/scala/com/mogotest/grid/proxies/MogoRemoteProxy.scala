package com.mogotest.grid.proxies

import org.openqa.grid.common.RegistrationRequest
import java.net.{URL, InetAddress}
import com.mogotest.grid.com.mogotest.grid.renderers.MogoConsoleRenderer
import org.openqa.grid.internal.{TestSession, RemoteProxy, Registry}
import org.openqa.grid.selenium.proxy.DefaultRemoteProxy
import util.Random
import scala.collection.JavaConversions._
import scala.util.parsing.json._
import scalaj.http.{Http, HttpOptions}

class MogoRemoteProxy(request: RegistrationRequest, registry: Registry) extends DefaultRemoteProxy(request, registry)
{
  /**
   * Our nodes register with some internal EC2 hostname.  We want to display with the canonical hostname instead,
   * which will end up being the logical hostname we establish for that node.
   */
  lazy val canonicalRemoteURL =
  {
    val url = getRemoteHost
    var canonicalHost = InetAddress.getByName(remoteHost.getHost).getCanonicalHostName

    // Normalize the hostname if we can by stripping off the common domain portion.
    if (canonicalHost.endsWith(".mogotest.com"))
      canonicalHost = canonicalHost.split("\\.")(0)

    new URL(url.getProtocol, canonicalHost, url.getPort, url.getFile)
  }

  /**
   * Used for the HTML representation of the proxy in the console.
   */
  override def getHtmlRender() = new MogoConsoleRenderer(this)

  override def compareTo(other: RemoteProxy) : Int =
    if (other == null)
      -1
    else
    {
      val totalDiff = (getTotalUsed / getMaxNumberOfConcurrentTestSessions.toFloat) - (other.getTotalUsed / other.getMaxNumberOfConcurrentTestSessions.toFloat)

      // If both proxy have no running tests, randomly choose between them.
      if (totalDiff == 0)
      {
        val choices = List(-1, 1)
        choices(Random.nextInt(choices.length))
      }
      else
        // We need to return an Int, so multiply by a factor of 10 so we retain some precision from our Float.
        (totalDiff * 1000).toInt
    }
}