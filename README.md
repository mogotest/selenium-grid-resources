# Introduction #

This project is a collection of Selenium Grid extensions that were used heavily by the Mogotest Web Consistency Testing service.  It
combines several hub APIs provided as custom servlets with a custom proxy to make working with nodes easier.

The code is licensed under the Apache License, version 2.0.  This code is being provided as is. It was developed for internal use by Mogotest, but Mogotest
is no longer in business. The project may be useful to other Selenium Grid installations, but the project is in search of a new maintainer.

# The Code #

The project was originally an early prototype of Scala to be used within Mogotest.  It's not very pretty Scala, but the code is fairly clear.
It's mostly what you'd expect if a Java developer were to start writing Scala.  Rewriting this project to use Java natively would be trivial and is
recommended, if for no other reason than to cut down on the number of dependencies.  However, since Mogotest is no longer in business this work
would need to originate from elsewhere.

## Building ##

This project is written Scala.  Scala 2.10.x was used, but the only necessary dependency is sbt 0.13.x.  To build the project, simply run:

```
#!bash

$ sbt package
```

The resulting artifact will be in the `target/` directory, categorized by scala version.  E.g., `target/scala-2.10/mogotest-selenium-grid-resources_2.10-1.6.0.jar`.

## Running ##

### Dependencies ###

The compiled JAR will be skinny, so you'll need to manually include its dependencies when running.  The project was never written for mass consumption and internal
Mogotest tooling made library management like this easier.  The only needed dependencies are the core Scala libraries and the [scalaj-http](https://github.com/scalaj/scalaj-http)
HTTP library.  


### Hub ###

Loading the project on your Selenium Grid hub is fairly straightforward.  First, this JAR and its dependencies must be loaded on the classpath.  Then each of the
included servlets must be enumerated and specified on the start-up command.  E.g.,

```
#!bash

$ java -cp "selenium-server-standalone.jar:/opt/scala/lib/scala-library.jar:mogotest-selenium-grid-resources_2.10-1.6.0.jar:~/.ivy2/cache/org.scalaj/scalaj-http_2.10/jars/scalaj-http_2.10-0.3.10.jar" \ 
       org.openqa.grid.selenium.GridLauncher \ 
       -role hub \ 
       -servlets com.mogotest.grid.servlets.Console,com.mogotest.grid.servlets.Sessions,com.mogotest.grid.servlets.AvailableBrowsers,com.mogotest.grid.servlets.Proxies
```


### Node ###

The Selenium Grid node does not need to load the library directly, but it must inform the hub about its custom proxy implementation.  Since the hub already has the necessary
classes loaded (providing you followed the steps in the 'Hub' section), both sides should have everything they need.  In order to customize the proxy, you would add something
like the following to your node_config.json file:

```
#!json

{
  "configuration":
    {
      "proxy": "com.mogotest.grid.proxies.MogoRemoteProxy"
    }
}
```

## API ##

The goal of the servlets are to provides a few APIs to make managing and working with the grid easier.  The following APIs are available:

* GET /grid/admin/AvailableBrowsers &mdash; searches through all nodes and reconciles the per-browser session limit against the per-node max session limit and the number of running sessions, providing a realistic (rather than idealistic) total count of available slots per browser.
* GET /grid/admin/Proxies &mdash; provides a simple look-up from a node's hostname to its grid ID.
* DELETE /grid/admin/Proxies?id=<id> &mdash; unregisters the proxy with the provided ID from the hub.
* GET /grid/admin/Sessions &mdash; lists all running sessions, grouped by node.
* GET /grid/admin/Sessions?sessionID=<sessionID> &mdash; lists all running sessions on the node running the specified session.

## Console ##

This project provides a very simple, but fast hub console that groups proxies by browser.  While not pretty, it allows for fast lookup to see which nodes are currently
running sessions for any given browser.

You can access it at `http://<my_hub>:4444/grid/admin/Console`

## Caveats ##

Much like Selenium, this project expects you're running on a trusted network.  One of the APIs allows unauthorized access to unregister nodes from your hub.  If you make this
publicly available, expect to have a bad day.  No attempt was made at securing this because the code is embedded in the Selenium Grid hub, which shouldn't be run on an
untrusted network to begin with.

